<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', function () {
    return view('adminlte.master');
});
Route::get('/data-table', function () {
    return view('items.data-table');
});
Route::get('/table', function () {
    return view('items.table');
});

// CRUD
// CREATE
Route::get('/cast/create', 'App\Http\Controllers\CastController@create');
Route::post('/cast', 'App\Http\Controllers\CastController@store');
// READ
Route::get('/cast', 'App\Http\Controllers\CastController@index');
Route::get('/cast/{cast_id}', 'App\Http\Controllers\CastController@show');
// UPDATE
Route::get('/cast/{cast_id}/edit', 'App\Http\Controllers\CastController@edit');
Route::put('/cast/{cast_id}', 'App\Http\Controllers\CastController@update');
// DELETE
Route::delete('/cast/{cast_id}', 'App\Http\Controllers\CastController@destroy');
