@extends('adminlte.master')
@section('data-content')
Halaman List Cast
@endsection
@section('data-section')
<a href="/cast/create" class="btn btn-primary btn-sm my-3" >Create Data</a>
<table class="table">
  <thead class="theas-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
  
    </tr>
  </thead>
  <tbody>
    @forelse($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                @csrf    
                @method('delete')
                     <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                     <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                     <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
           
        </td>
        </tr>
    @empty
    <tr>
        <td>Belum ada data</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection