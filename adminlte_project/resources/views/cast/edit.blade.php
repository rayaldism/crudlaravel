@extends('adminlte.master')
@section('data-content')
Halaman Tambah Cast
@endsection
@section('data-section')
<form action ="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Nama</label>
    <input name="nama" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input name="umur" value="{{$cast->umur}}" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection