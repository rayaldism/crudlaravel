@extends('adminlte.master')
@section('data-content')
Halaman Tambah Cast
@endsection
@section('data-section')
<form action ="/cast" method="post">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input name="umur" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" id="" cols="30" rows="10" class="form-control"></textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection